#ifndef CONFIG_USER_H
#define CONFIG_USER_H
/* Debounce reduces chatter (unintended double-presses) - set 0 if debouncing is not needed */
#undef DEBOUNCE
#define DEBOUNCE 2

#define ONESHOT_TAP_TOGGLE 5
#define ONESHOT_TIMEOUT 5000
#define TAPPING_TERM 200
#define TAPPING_TOGGLE 2

#endif
